package task04;

enum ClothingSize {
    XS(44), S(46), M(48), L(50), XL(52), XXL(54), XXXL(56);

    private int clothingSize;

    ClothingSize(int clothingSize) {
        this.clothingSize = clothingSize;
    }

    public int getClothingSize() {
        return clothingSize;
    }

}
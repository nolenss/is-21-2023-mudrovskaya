package task04;
import java.util.Objects;
public class Person {
    private Gender gender;
    private ClothingSize clothingSize;

    public Person(Gender gender, ClothingSize clothingSize) {
        this.gender = gender;
        this.clothingSize = clothingSize;
    }
    public Gender getGender() {
        return gender;
    }

    public ClothingSize getClothingSize() {
        return clothingSize;
    }

    @Override
    public boolean equals(Object o) {

        if (!(o instanceof Person)) return false;
        if (this == o) return true;
        Person person = (Person) o;
        return Objects.equals(getGender(), getGender() == person.getGender() &&
                getClothingSize() == person.getClothingSize());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGender(), getClothingSize());
    }
}
